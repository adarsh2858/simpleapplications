var todosList = document.getElementById("todos-list");

let todosCount = 1;
let title, description;
let itemsArray = localStorage.getItem("items")
  ? JSON.parse(localStorage.getItem("items"))
  : [];

localStorage.setItem("items", JSON.stringify(itemsArray));
const data = JSON.parse(localStorage.getItem("items"));

document
  .getElementById("remove-btn")
  ?.addEventListener("click", function (event) {
    removeTodo(event.target);
  });

$("#edit-btn").on("click", (event) => {
  editTodo(event.target);
});

const addTodo = () => {
  let li = document.createElement("li");
  title = document.createElement("h1");
  description = document.createElement("p");
  let titleWithEditAndDelete = document.createElement("div");
  let editButton = document.createElement("button");
  let deleteButton = document.createElement("button");
  let editImage = document.createElement("img");
  let deleteImage = document.createElement("img");

  editImage.src = "edit_icon.png";
  editImage.alt = "Edit";
  editImage.width = "24";
  editButton.classList.add("edit-btn");
  deleteImage.src = "delete_icon.png";
  deleteImage.alt = "Delete";
  deleteImage.width = "24";
  deleteButton.classList.add("remove-btn");

  editButton.appendChild(editImage);
  deleteButton.appendChild(deleteImage);

  li.classList.add("todo");
  li.classList.add("m-2");
  title.classList.add("text-2xl");
  titleWithEditAndDelete.classList.add("flex", "mb-4");
  editButton.classList.add("px-2", "mx-2");

  titleWithEditAndDelete.appendChild(title);
  titleWithEditAndDelete.appendChild(editButton);
  titleWithEditAndDelete.appendChild(deleteButton);

  li.appendChild(titleWithEditAndDelete);
  li.appendChild(description);

  todosList.append(li);

  deleteButton.addEventListener("click", function (event) {
    removeTodo(event.target);
  });

  editButton.addEventListener("click", (event) => {
    editTodo(event.target);
  });
};

const addNewTodo = () => {
  addTodo();
  title.innerHTML = `${++todosCount}. ` + $("#todo-title").val();
  description.innerHTML = $("#todo-description").val();
  itemsArray.push({
    title: title.innerHTML,
    description: description.innerHTML,
  });
  localStorage.setItem("items", JSON.stringify(itemsArray));
  alert("New Todo Added");
}

const addExistingTodos = (value, index) => {
  addTodo();
  title.innerText = value.title;
  description.innerText = value.description;
  todosCount = index + 2;
}

itemsArray.forEach(addExistingTodos);

const resetTodoFields = () => {
  if (confirm("Reset Todo?")) {
    $("#todo-title").innerText = "";
    $("#todo-description").innerText = "";
  }
};

const removeTodo = (target) => {
  const targetTodo = target.parentNode.parentNode.parentNode;
  targetTodo.remove();
  console.log("Deleted todo");
  todosCount--;
};

const editTodo = (target) => {
  const targetLi = target.parentNode.parentNode.parentNode;
  const newTitle = prompt("Enter todo title");
  const newDescription = prompt("Enter todo description");

  if (newTitle)
    targetLi.querySelector("div h1").innerText =
      `${todosCount - 1}. ` + newTitle;

  if (newDescription) targetLi.querySelector("p").innerText = newDescription;
};
