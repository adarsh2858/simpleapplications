class Stack {
  constructor() {
    this.items = [];
  }

  push(element) {
    this.items.push(element);
  }

  peek() {
    if (this.items.length <= 0) return null;

    return this.items[this.items.length - 1];
  }

  pop() {
    if (this.items.length <= 0) return "Underflow";

    return this.items.pop();
  }
}

const numbersList = document.querySelectorAll(".number");
const displayResult = document.getElementById("result");
const operationList = document.getElementsByClassName("operation");
const equalsOperator = document.getElementById("equals");

for (let i = 0; i < numbersList.length; i++) {
  numbersList[i].addEventListener("click", () => {
    displayResult.value += numbersList[i].textContent;
  });
}

for (let i = 0; i < operationList.length; i++) {
  operationList[i].addEventListener("click", () => {
    switch (operationList[i].textContent) {
      case "+":
        displayResult.value += operationList[i].textContent;
        break;
      case "-":
        displayResult.value += operationList[i].textContent;
        break;
      case "*":
        displayResult.value += operationList[i].textContent;
        break;
      case "/":
        displayResult.value += operationList[i].textContent;
        break;
      default:
        alert("Enter valid operation");
    }
  });
}

equalsOperator.addEventListener("click", () => {
  const numberOfElements = displayResult.value.length;
  const operations = ["+", "-", "*", "/"];

  if (
    operations.includes(displayResult.value[0]) ||
    operations.includes(displayResult.value[numberOfElements - 1])
  ) {
    alert("Wrong expression");
    return;
  }

  let expressionStack = new Stack();

  expressionStack.push(parseInt(displayResult.value[0]));

  for (let i = 1; i < displayResult.value.length; i++) {
    const currentNumber = displayResult.value[i];

    if (operations.includes(expressionStack.peek())) {
      if (!operations.includes(currentNumber))
        expressionStack.push(parseInt(currentNumber));
      else alert("Wrong expression");
    } else {
      if (!operations.includes(currentNumber)) {
        let num = parseInt(currentNumber);
        num = expressionStack.pop() * 10 + num;
        expressionStack.push(num);
      } else expressionStack.push(currentNumber);
    }
  }
  calculateResult(expressionStack);
});

const calculateResult = (expressionStack) => {
  let result = expressionStack.items[0];

  for (var i = 1; i < expressionStack.items.length; i += 2) {
    switch (expressionStack.items[i]) {
      case "+":
        result += expressionStack.items[i + 1];
        break;

      case "-":
        result -= expressionStack.items[i + 1];
        break;

      case "*":
        result *= expressionStack.items[i + 1];
        break;

      case "/":
        result /= expressionStack.items[i + 1];
        break;

      default:
        alert("Something went wrong");
    }
  }

  displayResult.value = result;
};
